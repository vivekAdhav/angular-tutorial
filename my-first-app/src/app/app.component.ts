import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-first-app';
  oddNumbers: number[] = [];
  evenNumbers: number[] = [];
  onlastIntervalFired(firedInterval: number) {
    // console.log(firedInterval);
    if (firedInterval % 2 === 0) {
      this.evenNumbers.push(firedInterval);
    } else {
      this.oddNumbers.push(firedInterval);
    }
  }
}
