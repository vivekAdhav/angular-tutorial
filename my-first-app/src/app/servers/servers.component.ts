import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {

  username = 'andrue';
  secreteKey:boolean =false;
  secreteCode :string = 'Secret password : luna';
  log:any =[];
  constructor() {

  }
  onToggleSecrateKey(){
    this.secreteKey = !this.secreteKey;
    this.log.push(this.log.length + 1);
  }

  reset(){
    this.username = '';
  }

  ngOnInit() {
  }

}
